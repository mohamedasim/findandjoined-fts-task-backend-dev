using System;
using System.Data;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Text.Json.Serialization;
namespace StudentApiFindAndJoined.Controllers
{
    [ApiController]
    [Route("api/student")]
    public class CommonController : Controller
    {
        IConfiguration Configure;

        public CommonController (IConfiguration configuration){
             Configure=configuration;
         }
        [HttpGet("joins/list")]
        public ActionResult GetJoinedData(){

            string query= @"select * from students inner join Gender on Students.Gender = Gender.Id inner join Department on Students.Department = Department.Id";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table);
        }
        [HttpGet("between-percentage/all/from/{percentage}")]
        public ActionResult GetPercentage(int percentage){

            string query= @"select * from students inner join Gender on Students.Gender = Gender.Id 
inner join Department on Students.Department = Department.Id where MarksPercentage <='"+percentage+"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table);
        }
         [HttpGet("by-gender/{genderId}")]
        public ActionResult GetGender(int genderId){

            string query= @"select * from students inner join Gender on Students.Gender = Gender.Id 
inner join Department on Students.Department = Department.Id where Gender.Id <='"+genderId+"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table);
        }

         [HttpGet("by-department/{departmentId}")]
        public ActionResult GetDepartment(int departmentId){

            string query= @"select * from students inner join Gender on Students.Gender = Gender.Id 
inner join Department on Students.Department = Department.Id where Department.Id <='"+departmentId+"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table);
        }
        [HttpGet("count")]
        public ActionResult GetCountOfStudent(int departmentId){

            string query= @"select count(Name) from Students";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table);
        }
    }
}